import axios from 'axios';

export default axios.create({
  baseURL: 'https://api.rawg.io/api',
  params: {
    key: '15e8698b0e6a474b99db6eaf9a2eadda'
  }
});
