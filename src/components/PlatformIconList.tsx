import { FaWindows, FaPlaystation, FaXbox, FaApple, FaLinux, FaAndroid } from 'react-icons/fa';
import { MdPhoneIphone } from 'react-icons/md';
import { SiNintendo } from 'react-icons/si';
import { BsGlobe } from 'react-icons/bs';
import { HStack, Icon, Text } from '@chakra-ui/react';
import { Platform } from '../hooks/useGames';
import { IconType } from 'react-icons';

interface Props {
  platforms: Platform[]
};


// search-for: typescript index signature
const PlatformIconList = ({ platforms }: Props) => {

  const parentPlatformsIcon: {[key: string]: IconType} = {
    pc: FaWindows,
    playstation: FaPlaystation,
    xbox: FaXbox,
    mac: FaApple,
    linux: FaLinux,
    android: FaAndroid,
    ios: MdPhoneIphone,
    nintendo: SiNintendo,
    web: BsGlobe
  };

  return (
    <HStack marginY={1}>
      {
        platforms.map(({id, name, slug}) => (
          <Icon key={id} as={parentPlatformsIcon[slug]} color='gray.500' />
        ))
      }
    </HStack>
  );
}

export default PlatformIconList;